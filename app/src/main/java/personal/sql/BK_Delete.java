package personal.sql;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import personal.sql.DB.NoteDB;

final class BK_Delete extends AsyncTask<Integer,Void,Integer>{
    private Activity uiActivity;

    BK_Delete(Activity uiActivity) {
        this.uiActivity = uiActivity;
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Integer doInBackground(Integer... ids) {
        NoteDB noteDB = new NoteDB(this.uiActivity);
        return noteDB.deleteNote(ids[0]);
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param aBoolean The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Integer rowsDeleted) {
        Toast.makeText(this.uiActivity,"("+rowsDeleted+") rows deleted",Toast.LENGTH_SHORT).show();
    }
}
