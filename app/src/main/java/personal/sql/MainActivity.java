package personal.sql;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import personal.sql.Rep.Note;

/**
 * <ol type="I">
 * <li><a href="http://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html">developer.android.com/reference | SQLiteDatabase</a></li>
 * <li><a href="http://developer.android.com/reference/android/database/sqlite/SQLiteQueryBuilder.html">developer.android.com/reference | SQLiteQueryBuilder</a></li>
 * <li><a href="http://developer.android.com/reference/android/database/sqlite/SQLiteOpenHelper.html">developer.android.com/reference | SQLiteOpenHelper</a></li>
 * <li><a href="https://www.sqlite.org/docs.html">sqlite.org | Docs</a></li>
 * <li><a href="http://developer.android.com/reference/android/os/AsyncTask.html">developer.android.com/reference | AsyncTask</a></li>
 * </ol>
 * Database operations were called in ASyncTask instead of being called from inside the activity directly to avoid having the UI waiting for a Database call.
 */
public class MainActivity extends AppCompatActivity {
    private EditText editTextTitle;
    private EditText editTextDetails;
    private Button buttonAdd;
    private Button buttonViewNotes;
    private ListView listViewNotes;
    static final String INTENT_NOTE_NAME_ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }


    private void initComponents() {
        this.editTextTitle = (EditText) this.findViewById(R.id.edit_text_title);
        this.editTextDetails = (EditText) this.findViewById(R.id.edit_text_details);
        this.buttonAdd = (Button) this.findViewById(R.id.button_add);
        this.buttonViewNotes = (Button) this.findViewById(R.id.button_view);
        this.listViewNotes = (ListView) this.findViewById(R.id.list_view_notes);

        this.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = MainActivity.this.editTextTitle.getText().toString();
                String details = MainActivity.this.editTextDetails.getText().toString();
                Toast.makeText(MainActivity.this,"Adding",Toast.LENGTH_SHORT).show();
                new BK_Add(MainActivity.this).execute(new Note(title,details));
            }
        });

        this.buttonViewNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BK_View(MainActivity.this).execute();
            }
        });

        this.listViewNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Note noteSelected = (Note) MainActivity.this.listViewNotes.getItemAtPosition(position);
                int noteId = noteSelected.getId();
                MainActivity.this.startActivity(new Intent(MainActivity.this,NoteActivity.class).putExtra(INTENT_NOTE_NAME_ID,noteId));
            }
        });
    }

}
