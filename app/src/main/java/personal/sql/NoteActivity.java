package personal.sql;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class NoteActivity extends AppCompatActivity {
    private int noteId;
    private Button buttonDelete;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        this.init();
    }

    private void init() {
        this.noteId = this.getIntent().getIntExtra(MainActivity.INTENT_NOTE_NAME_ID,-1);
        this.buttonDelete = (Button) this.findViewById(R.id.note_button_delete);

        BK_ViewById bk_viewById = new BK_ViewById(this);
        bk_viewById.execute(this.noteId);

        this.buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BK_Delete bk_delete = new BK_Delete(NoteActivity.this);
                bk_delete.execute(NoteActivity.this.noteId);
                NoteActivity.this.finish();
            }
        });

    }

}
