package personal.sql;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.ListView;

import personal.sql.DB.NoteDB;
import personal.sql.Rep.Note;

final class BK_View extends AsyncTask<Void,Void,Note[]>{
    private Activity uiActivity;

    BK_View(Activity uiActivity) {
        this.uiActivity = uiActivity;
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Note[] doInBackground(Void... params) {
        NoteDB noteDB = new NoteDB(this.uiActivity);
        return noteDB.getAllNote();
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param notes The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Note[] notes) {
        ListView listViewNotes = (ListView) this.uiActivity.findViewById(R.id.list_view_notes);

        NoteAdapter noteAdapter = new NoteAdapter(this.uiActivity,notes);

        listViewNotes.setAdapter(noteAdapter);
        super.onPostExecute(notes);
    }
}
