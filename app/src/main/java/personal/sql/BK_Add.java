package personal.sql;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import personal.sql.DB.NoteDB;
import personal.sql.Rep.Note;

final class BK_Add extends AsyncTask<Note, Void, String>{
    private AppCompatActivity uiActivity;

    BK_Add(AppCompatActivity uiActivity) {
        this.uiActivity = uiActivity;
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p/>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected String doInBackground(Note... notes) {
        NoteDB noteDB = new NoteDB(uiActivity);
        long notesAdded = noteDB.addNote(notes[0]);
        return "("+notesAdded+") notes added.";
    }


    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param test The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(String test) {
        Toast.makeText(this.uiActivity,test,Toast.LENGTH_SHORT).show();
    }
}
