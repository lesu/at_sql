package personal.sql;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.TextView;

import personal.sql.DB.NoteDB;
import personal.sql.Rep.Note;

class BK_ViewById extends AsyncTask<Integer,Void,Note>{
    private Activity uiActivity;

    BK_ViewById(Activity uiActivity) {
        this.uiActivity = uiActivity;
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Note doInBackground(Integer... noteIds) {
        NoteDB noteDB = new NoteDB(this.uiActivity);
        return noteDB.getNote(noteIds[0]);
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param note The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Note note) {
        //http://stackoverflow.com/questions/7727808/android-resource-not-found-exception/13626327#13626327
        ((TextView)this.uiActivity.findViewById(R.id.note_text_view_id)).setText(String.valueOf(note.getId()));

        ((TextView)this.uiActivity.findViewById(R.id.note_text_view_title)).setText(note.getTitle());
        ((TextView)this.uiActivity.findViewById(R.id.note_text_view_details)).setText(note.getDetails());
    }
}
