package personal.sql.Rep;

final public class Note {
    private int id;
    private String title;
    private String details;

    private static final int DEFAULT_ID = 0;
    private static final String DEFAULT_Details = "";

    public Note(String title) {
        this(Note.DEFAULT_ID,title,Note.DEFAULT_Details);
    }

    public Note(String title, String details) {
        this(Note.DEFAULT_ID,title,details);
    }

    public Note(int id, String title, String details) {
        this.id = id;
        this.title = title;
        this.details = details;
    }



    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDetails(String details) {
        this.details = details;
    }



    public int getId() {
        return this. id;
    }

    public String getTitle() {
        return this. title;
    }

    public String getDetails() {
        return this. details;
    }
}
