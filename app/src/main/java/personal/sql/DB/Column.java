package personal.sql.DB;

import java.util.ArrayList;
import java.util.Arrays;

final class Column {
    private String name;
    private ColumnType type;
    private ArrayList<String> attributes;



    Column(String name, ColumnType type) {
        this.name = name;
        this.type = type;
        this.attributes = new ArrayList<String>();
    }

    Column(String name, ColumnType type, ArrayList<String> attributes) {
        this.name = name;
        this.type = type;
        this.attributes = attributes;
    }

    void setAttributes(ArrayList<String> attributes) {
        this.attributes = attributes;
    }

    void setAttributes(String[] attributes) {
        this.attributes = new ArrayList<String>(Arrays.asList(attributes));
    }

    void addAttribute(String attribute) {
        this.attributes.add(attribute);
    }

    void addAttributes(String... attributes) {
        for (int i = 0; i < attributes.length; i++) {
            this.attributes.add(attributes[i]);
        }
    }

    String getName() {
        return this.name;
    }

    ColumnType getType() {
        return this.type;
    }

    String[] getAttributes() {
        return this.attributes.toArray(new String[0]);
    }
}
