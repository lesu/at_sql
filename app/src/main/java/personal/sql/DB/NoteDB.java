package personal.sql.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import java.util.ArrayList;

import personal.sql.Rep.Note;


public final class NoteDB extends SQLiteOpenHelper{

    public NoteDB(Context context) {
        super(context, Meta.DB_NAME,null,Meta.DB_VERSION);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param sqLiteDatabase The database.
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        this.createNoteTable(sqLiteDatabase);
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



//    Note Specific methods and constants.

    class Meta {
        final static String DB_NAME = "NotesDB";
        final static int DB_VERSION = 1;
        final static String NOTE_TABLE_NAME = "note";
        final static String NOTE_TABLE_COL_1_ID_NAME = "id";
        final static String NOTE_TABLE_COL_2_TITLE_NAME = "title";
        final static String NOTE_TABLE_COL_3_DETAILS_NAME = "details";
    }

    private boolean createNoteTable(SQLiteDatabase sqLiteDatabase) {
        Table notes = new Table(Meta.NOTE_TABLE_NAME);
        Column id = new Column(Meta.NOTE_TABLE_COL_1_ID_NAME,ColumnType.INTEGER);
        id.addAttribute(ColumnAttribute.PRIMARY_KEY.getName());
        id.addAttribute(ColumnAttribute.AUTOINCREMENT.getName());
        notes.addComlumn(id);
        notes.addComlumn(new Column(Meta.NOTE_TABLE_COL_2_TITLE_NAME,ColumnType.TEXT));
        notes.addComlumn(new Column(Meta.NOTE_TABLE_COL_3_DETAILS_NAME,ColumnType.TEXT));

        String createTableString = this.createTable(notes);
        sqLiteDatabase.execSQL(createTableString);
        return true;
    }

    public long addNote(Note note) {
        SQLiteDatabase writableDatabase = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Meta.NOTE_TABLE_COL_2_TITLE_NAME, note.getTitle());
        contentValues.put(Meta.NOTE_TABLE_COL_3_DETAILS_NAME, note.getDetails());

        long id = writableDatabase.insert(Meta.NOTE_TABLE_NAME, null, contentValues);

        writableDatabase.close();
        return id;
    }

    public int deleteNote(Note note) {
        return this.deleteNote(note.getId());
    }

    public int deleteNote(int noteId) {
        SQLiteDatabase writableDatabase = this.getWritableDatabase();
        int numberOfDeletedRows = writableDatabase.delete(Meta.NOTE_TABLE_NAME, Meta.NOTE_TABLE_COL_1_ID_NAME + "=" + noteId, null);
        writableDatabase.close();
        return numberOfDeletedRows;
    }

    public Note[] getAllNote() {
        ArrayList<Note> notes = new ArrayList<Note>();
        SQLiteDatabase readableDatabase = this.getReadableDatabase();
        Cursor cursor = readableDatabase.query(Meta.NOTE_TABLE_NAME, null, null, null, null, null, Meta.NOTE_TABLE_COL_1_ID_NAME);

        if(cursor.moveToFirst()) {
            do {
                notes.add(new Note(cursor.getInt(0),cursor.getString(1),cursor.getString(2)));
            }
            while (cursor.moveToNext());
        }
        readableDatabase.close();
        return notes.toArray(new Note[0]);
    }

    public Note getNote(int noteId) {
        SQLiteDatabase readableDatabase = this.getReadableDatabase();
        Cursor cursor = readableDatabase.query(Meta.NOTE_TABLE_NAME, null, Meta.NOTE_TABLE_COL_1_ID_NAME+"="+noteId, null, null, null, Meta.NOTE_TABLE_COL_1_ID_NAME);
        cursor.moveToFirst();
        Note note = new Note(cursor.getInt(0),cursor.getString(1),cursor.getString(2));
        readableDatabase.close();
        return note;
    }

//    Generalized Methods
    private String createTable (Table table) {

        StringBuffer result = new StringBuffer();
        Column[] columns = table.getColumns();

        result.append("CREATE TABLE ");
        result.append(table.getName());
        result.append(" (");

        for (int i = 0; i < columns.length; i++) {
            result.append(columns[i].getName());
            result.append(" ");
            result.append(columns[i].getType());
            result.append(" ");
            {
                String[] attributes = columns[i].getAttributes();
                for (int j = 0; j < attributes.length; j++) {
                    result.append(attributes[j]);
                    if(j!=(attributes.length-1)){
                        result.append(" ");
                    }

                }
            }
            if(i!=(columns.length-1)){
                result.append(",");
            }
        }

        result.append(")");
        return result.toString();
    }
}
