package personal.sql.DB;

/**
 * <a href="https://www.sqlite.org/datatype3.html">sqlite.org | Datatypes In SQLite Version 3</a>
 */
public enum ColumnType {
    NULL,
    INTEGER,
    REAL,
    TEXT,
    BLOB
}
