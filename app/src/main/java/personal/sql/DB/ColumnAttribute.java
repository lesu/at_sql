package personal.sql.DB;

public enum ColumnAttribute {
    NOT_NULL("NOT NULL"),
    UNIQUE("UNIQUE"),
    PRIMARY_KEY("PRIMARY KEY"),
    FOREIGN_KEY("FOREIGN KEY"),
    AUTOINCREMENT("AUTOINCREMENT");

    private String name;

    ColumnAttribute(String name) {
        this.name = name;
    }

    String getName() {
        return this.name;
    }
}
