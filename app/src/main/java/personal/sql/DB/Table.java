package personal.sql.DB;

import java.util.ArrayList;
import java.util.Arrays;

class Table {
    private String name;
    private ArrayList<Column> columns;

    Table(String name) {
        this(name, new ArrayList<Column>());
    }

    Table(String name,Column[] columns) {
        this(name,new ArrayList<Column>(Arrays.asList(columns)));
    }

    Table(String name,ArrayList<Column> columns) {
        this.name = name;
        this.columns = columns;
    }

    void setName() {
        this.name = name;
    }

    void setColumns(ArrayList<Column> columns) {
        this.columns = columns;
    }

    void setColumns(Column[] columns) {
        this.columns = new ArrayList<Column>(Arrays.asList(columns));
    }

    void addComlumn(Column column) {
        this.columns.add(column);
    }

    Column getColumn(int index) {
        return this.columns.get(index);
    }

    String getName() {
        return this.name;
    }

    Column[] getColumns() {
        return columns.toArray(new Column[0]);
    }
}
